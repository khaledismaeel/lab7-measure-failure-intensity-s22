# Lab8 - measure failure intensity

## Introduction

Ok, during the last lab we were doing fuzz testing, this is a good tool to understand which inputs may break your app. But how to understand if you need to quality maitenance, and call an ambulance to save your product. There is the only way to understand it, you need to measure failure intensity of your product, and guess what we are going to do today. ***Let's roll!***

## Failure intensity

Well, this is a time when the name speaks for itself. Failure intensity is an amount of failures on your service during the unit of time(or code). Measuring it will help you to understand if something wrong goes with your service, to check it you may count number of 503/404 and other responses, compared to your normal 200s. We will talk about load testing later, but for now lets keep it simple. 

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab7 - Failure intensity
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab7-measure-failure-intensity-s22)
2. For todays lab you will need Apache Benchmark, on linux you can install it using shell command:
```sh
sudo apt install apache2-utils
```
3. And for testing we need some app, just use link from previous lab.
4. To test it you should run command like this one:
```sh
ab -n 20000 -c 100 -m "GET" _url_
```
This means that we are going to send 20000 requests, with 100 of them sending concurrently to the server. We have few lines like `Failed requests` and `Non-2xx responses`, which are exactly failures we're searching here. To measure the exact metric you should use just this formule, where MTTF is time between failures:
`FI = 1 / MTTF`.

## Homework

On my machine, I tried to run

``` bash
$ ab -n 20000 -c 400 -m "GET" "https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=k.ismaeel@innopolis.university"
```

No requests returned with a `2xx` response code. After inspecting the output with the `-v 5` option we noticed that all requests returned a `302` (a redirect). Later we ran the same command on Google Colab on the hopes that it might give more useful results, and it did! Apparently, `ab` depends on some operating system library for its requests and Google Colab's servers and libraries behave differently.

Finally, we ran

``` bash
$ ab -n 20000 -c 400 -m "GET" "https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=k.ismaeel@innopolis.university"
```
```
This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking script.google.com (be patient)
Completed 2000 requests
Completed 4000 requests
Completed 6000 requests
Completed 8000 requests
Completed 10000 requests
Completed 12000 requests
Completed 14000 requests
Completed 16000 requests
Completed 18000 requests
Completed 20000 requests
Finished 20000 requests


Server Software:        GSE
Server Hostname:        script.google.com
Server Port:            443
SSL/TLS Protocol:       TLSv1.2,ECDHE-ECDSA-CHACHA20-POLY1305,256,256
TLS Server Name:        script.google.com

Document Path:          /macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=k.ismaeel@innopolis.university
Document Length:        661 bytes

Concurrency Level:      400
Time taken for tests:   27.659 seconds
Complete requests:      20000
Failed requests:        5907
   (Connect: 0, Receive: 0, Length: 5907, Exceptions: 0)
Non-2xx responses:      477
Total transferred:      20935806 bytes
HTML transferred:       13381223 bytes
Requests per second:    723.10 [#/sec] (mean)
Time per request:       553.175 [ms] (mean)
Time per request:       1.383 [ms] (mean, across all concurrent requests)
Transfer rate:          739.19 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2   12  38.9      4     265
Processing:   356  494 216.3    420    5466
Waiting:      353  490 215.4    416    5444
Total:        362  506 223.0    426    5474

Percentage of the requests served within a certain time (ms)
  50%    426
  66%    507
  75%    562
  80%    585
  90%    704
  95%    801
  98%    953
  99%   1178
 100%   5474 (longest request)
```

We can calculate $$FI = \frac{1}{MTTF} = \frac{FR + NR}{T} = \frac{5907 + 477}{27956} = 0.23 \approx 0.2$$
